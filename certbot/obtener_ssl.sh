#!/bin/bash
# Basado en:
# https://github.com/wmnnd/nginx-certbot

DOMINIOS=(ejemplo.com www.ejemplo.com)
EMAIL=''  # Es muy recomendado añadir una dirección de correo válida.
STAGING=0 # Usar 1 si se está probando la configuración para evitar llegar al
          # límite de peticiones.

# Pasar el dominio como argumento
if [ $# == 1 ]; then DOMINIOS=($1); fi

# Habilitar el modo staging si es necesario
if [ $STAGING != "0" ]; then staging_arg="--staging"; fi

# Seleccionar el arg apropiado de correo
case "$EMAIL" in
  "") email_arg="--register-unsafely-without-email" ;;
  *) email_arg="--email $EMAIL" ;;
esac

echo " === Petición de certificado SSL a Let's Encrypt para $DOMINIOS ==="

# Unir los $DOMINIOS a -d args
dominios_args=''
for dominio in "${DOMINIOS[@]}"; do
  dominios_args="$dominios_args -d $dominio"
done

docker-compose run --rm --entrypoint "\
  certbot certonly --webroot -w /var/www/certbot \
    $staging_arg \
    $email_arg \
    $dominios_args \
    --agree-tos \
    --force-renewal" certbot
echo
