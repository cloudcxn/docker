#!/bin/bash
# El nombre del puente =< 15 caracteres y sin puntos
DOMINIO='ejemplo.lan'
NOMBRE_PUENTE='ejemplo.lan'
PUERTA_ENLACE='10.5.93.1'
SUBRED='10.5.93.0/24'

docker network create \
  --gateway=$PUERTA_ENLACE \
  --opt com.docker.network.bridge.name=$NOMBRE_PUENTE \
  --subnet=$SUBRED \
  $DOMINIO
