El archivo de configuración de Dolibarr se encuentra en:

`<ruta de instalación>/htdocs/conf/conf.php`

El directorio en donde se almacenan los archivos (guardados en un volumen de
docker) es:

`<ruta de instalación>/documents`
