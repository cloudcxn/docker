Docker
======

En este repositorio residen los archivos de código fuente, configuración, etc.
relacionados con Docker, Docker Compose, etc. cuyo desarrollo se beneficie por
el control de versiones.

Requerimientos
--------------

* Docker 20.10.18
* Docker Compose v2.10.2
