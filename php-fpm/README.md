PHP-FPM
=======

Este grupo de contenedores usa un NGINX como servidor web y PHP-FPM como
intérprete de PHP. Por defecto, usan la última versión disponible al
momento de ejecución.

## Ejecución
Crear los contenedores con sus respectivos volúmenes.

```shell
docker-compose up -d
```

Copiar el índice que será servido al volumen de PHP FPM.

```shell
docker cp index.php php-fpm:/var/www/html/
```

Copiar la configuración al volumen de NGINX.

```shell
docker cp nginx-php-fpm.conf nginx-php-fpm:/etc/nginx/conf.d/default.conf
```

Reiniciar NGINX para aplicar la nueva configuración.

```shell
docker restart nginx-php-fpm
```
