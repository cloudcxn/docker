"""
Referencias
https://docs.celeryproject.org/en/stable/getting-started/first-steps-with-celery.html
"""

from celery import Celery

# Usando IP
#app = Celery('tareas', broker='redis://10.5.93.41//')

# Usando DNS
app = Celery('tareas', broker='redis://redis.ejemplo.lan//')

@app.task
def sumar(x, y):
    return x + y
